using UnityEngine;
using System.Collections;

public class DummyAgentCharacter : MonoBehaviour
{
    public float walkDistance;

    private Animator animator; // The animator for the character
    private NavMeshAgent navAgent;

    // Use this for initialization
    private void Start()
    {
        SetUpAnimator();

        navAgent = GetComponentInChildren<NavMeshAgent>();
        SetAgentTarget();
    }

    private void SetAgentTarget()
    {
        var target = transform.position + transform.forward * walkDistance;
        navAgent.SetDestination(target);
    }

    void Update()
    {
        float forward = navAgent.speed * 0.5f;
        animator.SetFloat("Forward", forward, 0.1f, Time.deltaTime);
    }

    private void SetUpAnimator()
    {
        // this is a ref to the animator component on the root.
        animator = GetComponent<Animator>();

        // we use avatar from a child animator component if present
        // (this is to enable easy swapping of the character model as a child node)
        foreach (var childAnimator in GetComponentsInChildren<Animator>())
        {
            if (childAnimator != animator)
            {
                animator.avatar = childAnimator.avatar;
                Destroy(childAnimator);
                break;
            }
        }
    }
}
